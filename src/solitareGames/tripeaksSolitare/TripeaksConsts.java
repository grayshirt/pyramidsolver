package solitareGames.tripeaksSolitare;

/**
 * Contains constant values relating to Tripeaks solitare.
 */
public class TripeaksConsts {
    public final static int N_ROWS = 4, N_POSITIONS = 28;

    private TripeaksConsts() {}
}
