package solitareGames.tripeaksSolitare;

import playingCards.Card;
import playingCards.Deck;
import solitareGames.GameState;

import java.util.ArrayList;

/**
 * Represents any state of a game of Tripeaks solitare.
 */
public class TripeaksGameState implements GameState<TripeaksGameState> {

    private final Board board;
    private final Deck stock;
    private final Card topCard;
    private int cachedHash;

    // Constructors

    /**
     * Constructs a TripeaksGameState representing the start of a new game.
     * <p>
     * @param deck  the Deck from which to deal the game
     * @throws IllegalArgumentException if deck does not contain enough cards
     */
    public TripeaksGameState(Deck deck) {
        board = new Board(deck);
        stock = deck;
        topCard = stock.dealTop();
    }

    private TripeaksGameState(Board board, Deck stock, Card topCard) {
        this.board = board;
        this.stock = stock;
        this.topCard = topCard;
    }

    // Implementations of GameState interface
    public int estimatedCostToGoal() {
        return board.nCards();
    }

    public boolean isGoal() {
        return board.isEmpty();  // Relaxed rules; some require to empty stock and discard as well
    }

    public Iterable<TripeaksGameState> neighbors() {
        // Two kinds of moves: match card in play to board; deal top of stock to card in play
        final ArrayList<TripeaksGameState> neighboringStates = new ArrayList<>(9);  // max eight board matches, one deal
        if (topCard != null) {
            for (Board.Position p : board.matchesFor(topCard)) {
                Board neighborBoard = board.removeCardAt(p);
                Deck neighborStock = new Deck(stock);
                Card neighborCard = board.cardAt(p);
                TripeaksGameState neighborState = new TripeaksGameState(neighborBoard, neighborStock, neighborCard);
                neighboringStates.add(neighborState);
            }
        }
        if (!stock.isEmpty()) {
            Board neighborBoard = new Board(board);
            Deck neighborStock = new Deck(stock);
            Card neighborCard = neighborStock.dealTop();
            TripeaksGameState neighborState = new TripeaksGameState(neighborBoard, neighborStock, neighborCard);
            neighboringStates.add(neighborState);
        }
        return neighboringStates;
    }

    // Boilerplate
    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || getClass() != other.getClass()) {
            return false;
        }
        TripeaksGameState that = (TripeaksGameState) other;
        if (!board.equals(that.board)) {
            return false;
        }
        if (!stock.equals(that.stock)) {
            return false;
        }
        return (topCard != null ? topCard.equals(that.topCard) : that.topCard == null);
    }

    @Override
    public int hashCode() {
        if (cachedHash == 0) {
            int result = board.hashCode();
            result = 31 * result + stock.hashCode();
            result = 31 * result + (topCard != null ? topCard.hashCode() : 0);
            cachedHash = result;
        }
        return cachedHash;
    }

    public String toString() {
        String[] componentStrings = new String[3];
        componentStrings[0] = board.toString();
        componentStrings[1] = String.format("Stock (%d): %s", stock.size(), stock);
        componentStrings[2] = String.format("Card to play: %s", topCard != null ? topCard : "  ");
        return String.join("\n", componentStrings);
    }
}