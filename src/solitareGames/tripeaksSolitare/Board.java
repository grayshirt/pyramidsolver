package solitareGames.tripeaksSolitare;

import playingCards.Card;
import playingCards.Deck;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.StringJoiner;

class Board {

    public class Position {

        public final int row, col;

        private Position(int row, int col) {
            if (row < 0 || row >= TripeaksConsts.N_ROWS || col < 0
                || (row < 2 && col > (row + 1) * 3) || (row >= 2 && col > row + 7)) {
                throw new IllegalArgumentException("position invalid");
            }
            this.row = row;
            this.col = col;
        }

        // Boilerplate
        @Override
        public boolean equals(Object other) {
            if (this == other) {
                return true;
            }
            if (this.getClass() != other.getClass()) {
                return false;
            }
            Position that = (Position) other;
            return row == that.row && col == that.col;
        }

        public String toString() {
            return String.format("row %d, col %d", row, col);
        }
    }

    private final Card[][] cards;  // represent removed cards with null
    private Position[] cachedPositions;
    private int cachedHash, nCards;

    // Constructors
    public Board(Deck deck) {
        this();
        if (deck.size() < TripeaksConsts.N_POSITIONS) {
            throw new IllegalArgumentException(
                String.format("deck contains %d cards < %d required", deck.size(), TripeaksConsts.N_POSITIONS)
            );
        }
        for (Position p : allPositions()) {
            cards[p.row][p.col] = deck.dealTop();
        }
        nCards = TripeaksConsts.N_POSITIONS;
    }

    public Board(Board other) {
        this();
        for (Position p : allPositions()) {
            cards[p.row][p.col] = other.cardAt(p);
        }
        nCards = other.nCards;
    }

    private Board() {
        cards = new Card[][] {
                new Card[3],
                new Card[6],
                new Card[9],
                new Card[10]
        };
    }

    // Internals
    private Position[] allPositions() {
        if (cachedPositions == null) {
            cachedPositions = new Position[TripeaksConsts.N_POSITIONS];
            int i = 0;
            // First three rows
            for (int row = 0; row < 3; ++row) {
                for (int col = 0; col < (row + 1) * 3; ++col) {
                    cachedPositions[i++] = new Position(row, col);
                }
            }
            // Fourth row of ten positions
            for (int col = 0; col < 10; ++col) {
                cachedPositions[i++] = new Position(3, col);
            }
        }
        return cachedPositions;
    }

    private boolean positionIsExposed(Position p) {
        final boolean exposed;
        if (p.row == TripeaksConsts.N_ROWS - 1) {
            exposed = true;
        }
        else {
            int blockerCol;
            switch (p.row) {
                case 2:
                    blockerCol = p.col;
                    break;
                case 1:
                    blockerCol = p.col + (p.col / 2);
                    break;
                default:
                    blockerCol = p.col * 2;
            }
            exposed = cards[p.row + 1][blockerCol] == null && cards[p.row + 1][blockerCol + 1] == null;
        }
        return exposed;
    }

    private Collection<Position> exposedPositions() {
        final ArrayList<Position> exposed = new ArrayList<>();
        for (Position p : allPositions()) {
            if (positionIsExposed(p) && cardAt(p) != null) {
                exposed.add(p);
            }
        }
        return exposed;
    }

    // Public non-mutating methods
    public Card cardAt(Position p) {
        return cards[p.row][p.col];
    }

    public boolean isEmpty() {
        for (Position p : allPositions()) {
            if (cardAt(p) != null) {
                return false;
            }
        }
        return true;
    }

    public Iterable<Position> matchesFor(Card card) {
        final ArrayList<Position> matchPositions = new ArrayList<>();
        for (Position p : exposedPositions()) {
            final int absDiff = Math.abs((cardAt(p).rank) - (card.rank));
            if (absDiff == 1 || absDiff == 12) {
                matchPositions.add(p);
            }
        }
        return matchPositions;
    }

    public int nCards() {
        return nCards;
    }

    public Board removeCardAt(Position p) {
        if (cardAt(p) == null) {
            throw new IllegalArgumentException(
                    String.format("position %s is empty", p)
            );
        }
        else if (!positionIsExposed(p)) {
            throw new IllegalArgumentException(String.format("Card at %s is not exposed", p));
        }
        final Board other = new Board(this);
        other.cards[p.row][p.col] = null;
        return other;
    }

    // Boilerplate
    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || getClass() != other.getClass()) {
            return false;
        }
        Board that = (Board) other;
        return Arrays.deepEquals(cards, that.cards);
    }

    @Override
    public int hashCode() {
        if (cachedHash == 0) {
            cachedHash = Arrays.deepHashCode(cards);
        }
        return cachedHash;
    }

    public String toString() {
        // This is really bad but I don't know a better approach off the top of my head
        final String[] rowStrings = new String[TripeaksConsts.N_ROWS];
        rowStrings[0] = String.format(
                "      %s          %s          %s      ",
                cards[0][0] != null ? cards[0][0] : "  ",
                cards[0][1] != null ? cards[0][1] : "  ",
                cards[0][2] != null ? cards[0][2] : "  "
        );
        rowStrings[1] = String.format(
                "    %s  %s      %s  %s      %s  %s    ",
                cards[1][0] != null ? cards[1][0] : "  ",
                cards[1][1] != null ? cards[1][1] : "  ",
                cards[1][2] != null ? cards[1][2] : "  ",
                cards[1][3] != null ? cards[1][3] : "  ",
                cards[1][4] != null ? cards[1][4] : "  ",
                cards[1][5] != null ? cards[1][5] : "  "
        );
        // row [2]
        StringJoiner rowTwoJoiner = new StringJoiner("  ");
        for (Card card : cards[2]) {
            if (card != null) {
                rowTwoJoiner.add(card.toString());
            }
            else {
                rowTwoJoiner.add("  ");
            }
        }
        rowStrings[2] = "  " + rowTwoJoiner.toString() + "  ";
        // row [3]
        StringJoiner rowThreeJoiner = new StringJoiner("  ");
        for (Card card : cards[3]) {
            if (card != null) {
                rowThreeJoiner.add(card.toString());
            }
            else {
                rowThreeJoiner.add("  ");
            }
        }
        rowStrings[3] = rowThreeJoiner.toString();
        return String.join("\n", rowStrings);
    }
}
