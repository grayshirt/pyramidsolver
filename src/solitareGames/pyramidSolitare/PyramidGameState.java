package solitareGames.pyramidSolitare;

import java.util.ArrayList;

import common.UnorderedPair;
import playingCards.*;
import solitareGames.GameState;

/**
 * Represents any state of a game of Pyramid solitare.
 */
public class PyramidGameState implements GameState<PyramidGameState> {

    private final Board board;
    private final Deck stock;
    private final Deck discard;
    private final int stockCycles;
    private int cachedHash;

    // Constructors

    /**
     * Constructs a PyramidGameState representing the start of a new game.
     * <p>
     * @param deck  the Deck from which to deal the game
     * @param rows  the number of rows to deal in the pyramid
     * @param stockCycles  the number of times the stock may be recycled from the discard pile
     * @throws IllegalArgumentException if deck does not contain enough cards
     */
    public PyramidGameState(Deck deck, int rows, int stockCycles) {
        board = new Board(deck, rows);
        stock = deck;
        discard = new Deck();
        this.stockCycles = stockCycles;
    }

    private PyramidGameState(Board board, Deck stock, Deck discard, int stockCycles) {
        this.board = board;
        this.discard = discard;
        this.stock = stock;
        this.stockCycles = stockCycles;
    }

    // Implementations of GameState interface
    public int estimatedCostToGoal() {
        return board.nCards();
    }

    public boolean isGoal() {
        return board.isEmpty();  // Relaxed rules; some require to empty stock and discard as well
    }

    public Iterable<PyramidGameState> neighbors() {
        // TODO: Condense or at least encapsulate this into separate methods
        // Nine kinds of moves -- match pair on board, match board with top of stock, match board with top of discard,
        // match top of stock to top of discard, deal top of stock to top of discard, cycle discard to stock if empty
        final ArrayList<PyramidGameState> neighboringStates = new ArrayList<>();
        // 1/9 Moves consisting of matching pairs on the pyramid
        for (UnorderedPair<Board.Position> pair : board.matchingExposedPairs()) {
            final Board neighborBoard = board.removeCardsAt(pair.foo, pair.bar);
            final Deck neighborStock = new Deck(stock), neighborDiscard = new Deck(discard);
            final PyramidGameState neighbor = new PyramidGameState(neighborBoard, neighborStock, neighborDiscard, stockCycles);
            neighboringStates.add(neighbor);
        }
        // 2/9 Moves matching top of stock to board (if stock non-empty)
        if (!stock.isEmpty()) {
            Iterable<Board.Position> matches = board.matchesFor(stock.peekTop());
            for (Board.Position p : matches) {
                final Board neighborBoard = board.removeCardAt(p);
                final Deck neighborStock = new Deck(stock), neighborDiscard = new Deck(discard);
                neighborStock.dealTop();  // remove top card matched with board
                final PyramidGameState neighbor = new PyramidGameState(neighborBoard, neighborStock, neighborDiscard, stockCycles);
                neighboringStates.add(neighbor);
            }
        }
        // 3/9 Moves matching top of discard to board (if discard non-empty)
        if (!discard.isEmpty()) {
            final Iterable<Board.Position> matches = board.matchesFor(discard.peekTop());
            for (Board.Position p : matches) {
                final Board neighborBoard = board.removeCardAt(p);
                final Deck neighborStock = new Deck(stock), neighborDiscard = new Deck(discard);
                neighborDiscard.dealTop();  // remove top card matched with board
                final PyramidGameState neighbor = new PyramidGameState(neighborBoard, neighborStock, neighborDiscard, stockCycles);
                neighboringStates.add(neighbor);
            }
        }
        // 4/9 Match top of stock to top of discard
        if (!stock.isEmpty() && !discard.isEmpty()) {
            final Card topOfStock = stock.peekTop(), topOfDiscard = discard.peekTop();
            if (topOfStock.rank + topOfDiscard.rank == 13) {
                final Board neighborBoard = new Board(board);
                final Deck neighborStock = new Deck(stock), neighborDiscard = new Deck(discard);
                neighborStock.dealTop();
                neighborDiscard.dealTop();
                final PyramidGameState neighbor = new PyramidGameState(neighborBoard, neighborStock, neighborDiscard, stockCycles);
                neighboringStates.add(neighbor);
            }
        }
        // 5/9 Remove K from board
        for (Board.Position p : board.exposedKings()) {
            final Board neighborBoard = board.removeCardAt(p);
            final Deck neighborStock = new Deck(stock), neighborDiscard = new Deck(discard);
            final PyramidGameState neighbor = new PyramidGameState(neighborBoard, neighborStock, neighborDiscard, stockCycles);
            neighboringStates.add(neighbor);
        }
        // 6/9 Remove K from top of stock
        if (!stock.isEmpty() && stock.peekTop().rank == 13) {
            final Board neighborBoard = new Board(board);
            final Deck neighborStock = new Deck(stock), neighborDiscard = new Deck(discard);
            neighborStock.dealTop();
            final PyramidGameState neighbor = new PyramidGameState(neighborBoard, neighborStock, neighborDiscard, stockCycles);
            neighboringStates.add(neighbor);
        }
        // 7/9 Remove K from top of discard
        if (!discard.isEmpty() && discard.peekTop().rank == 13) {
            final Board neighborBoard = new Board(board);
            final Deck neighborStock = new Deck(stock), neighborDiscard = new Deck(discard);
            neighborDiscard.dealTop();
            final PyramidGameState neighbor = new PyramidGameState(neighborBoard, neighborStock, neighborDiscard, stockCycles);
            neighboringStates.add(neighbor);
        }
        // 8/9 Deal stock to discard
        if (!stock.isEmpty()) {
            final Board neighborBoard = new Board(board);
            final Deck neighborStock = new Deck(stock), neighborDiscard = new Deck(discard);
            neighborDiscard.addTop(neighborStock.dealTop());
            final PyramidGameState neighbor = new PyramidGameState(neighborBoard, neighborStock, neighborDiscard, stockCycles);
            neighboringStates.add(neighbor);
        }
        // 9/9 Recycle discard to stock
        if (stock.isEmpty() && !discard.isEmpty() && stockCycles > 0) {
            final Board neighborBoard = new Board(board);
            final Deck neighborStock = new Deck(), neighborDiscard = new Deck(discard);
            while (!neighborDiscard.isEmpty()) {
                neighborStock.addTop(neighborDiscard.dealTop());
            }
            final PyramidGameState neighbor = new PyramidGameState(neighborBoard, neighborStock, neighborDiscard, stockCycles - 1);
            neighboringStates.add(neighbor);
        }
        return neighboringStates;
    }

    private int cardsToDeal() {
        return stock.size() + stockCycles * (stock.size() + discard.size());
    }

    // Boilerplate
    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || getClass() != other.getClass()) {
            return false;
        }
        final PyramidGameState that = (PyramidGameState) other;
        if (stockCycles != that.stockCycles) {
            return false;
        }
        if (!board.equals(that.board)) {
            return false;
        }
        if (!stock.equals(that.stock)) {
            return false;
        }
        return discard.equals(that.discard);
    }

    @Override
    public int hashCode() {
        if (cachedHash == 0) {
            int result = board.hashCode();
            result = 31 * result + stock.hashCode();
            result = 31 * result + discard.hashCode();
            cachedHash = result;
        }
        return cachedHash;
    }

    public String toString() {
        String[] componentStrings = new String[3];
        componentStrings[0] = board.toString();
        componentStrings[1] = String.format("Stock (%d, %d cycles): %s", stock.size(), stockCycles, stock);
        componentStrings[2] = String.format("Discard (%d): %s", discard.size(), discard);
        return String.join("\n", componentStrings);
    }
}