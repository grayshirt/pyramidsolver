package solitareGames.pyramidSolitare;

/**
 * Contains constant values relating to Pyramid solitare.
 */
public class PyramidConsts {

    public static final int N_ROWS_STANDARD = 7;
    public static final int STOCK_CYCLES_NONE = 0;
    public static final int STOCK_CYCLES_RELAXED = 2;

    private PyramidConsts() {}
}