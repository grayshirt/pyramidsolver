package solitareGames.pyramidSolitare;

import java.util.*;
import java.util.stream.IntStream;

import common.UnorderedPair;
import playingCards.*;

class Board {

    // Convenience for calls between Board and related classes
    // Private constructor ensures Position valid for Board which created it; GameState receives Positions only
    // from Board methods
    public class Position {
        public final int row, col;
        private int cachedHash;

        // Constructor
        private Position(int row, int col) {
            if (row < 0 || row > nRows - 1 || col < 0 || col > row) {
                throw new IllegalArgumentException(
                        String.format("row %d, col %d invalid", row, col)
                );
            }
            this.row = row;
            this.col = col;
        }

        // Boilerplate
        @Override
        public boolean equals(Object other) {
            if (this == other) {
                return true;
            }
            if (this.getClass() != other.getClass()) {
                return false;
            }
            Position that = (Position) other;
            return row == that.row && col == that.col;
        }

        public int hashCode() {
            if (cachedHash == 0) {
                cachedHash = Arrays.deepHashCode(cards);
            }
            return cachedHash;
        }

        public String toString() {
            return String.format("row %d, col %d", row, col);
        }
    }

    private final Card[][] cards;  // represent removed cards with null
    private Position[] cachedPositions;
    private int cachedHash, nCards;
    private final int nPositions, nRows;

    // Constructors
    public Board(Deck deck, int nRows) {
        if (nRows < 1) {
            throw new IllegalArgumentException("nRows must be > 0");
        }
        this.nRows = nRows;
        nPositions = IntStream.rangeClosed(1, nRows).sum();
        if (deck.size() < nPositions) {
            throw new IllegalArgumentException(
                    String.format("deck contains %d cards; %d required", deck.size(), nPositions)
            );
        }
        // Must initialize rows; can't use allPositions() yet
        cards = new Card[nRows][];
        for (int row = 0; row < nRows; ++row) {
            cards[row] = new Card[row + 1];
            for (int col = 0; col < row + 1; ++col) {
                cards[row][col] = deck.dealTop();
            }
        }
        nCards = nPositions;
    }

    public Board(Board other) {
        this(other.nRows);
        for (Position p : allPositions()) {
            final Card card = other.cardAt(p);
            cards[p.row][p.col] = card;
            if (card != null) {
                ++nCards;
            }
        }
    }

    private Board(int nRows) {
        if (nRows < 1) {
            throw new IllegalArgumentException("nRows must be > 0");
        }
        this.nRows = nRows;
        nPositions = IntStream.rangeClosed(1, nRows).sum();
        cards = new Card[nRows][];
        for (int row = 0; row < nRows; ++row) {
            cards[row] = new Card[row + 1];
        }
        nCards = 0;
    }

    // Internals
    private Position[] allPositions() {
        if (cachedPositions == null) {
            cachedPositions = new Position[nPositions];
            int i = 0;
            for (int row = 0; row < nRows; ++row) {
                for (int col = 0; col <= row; ++col) {
                    cachedPositions[i++] = new Position(row, col);
                }
            }
        }
        return cachedPositions;
    }

    private Card cardAt(Position p) {
        return cards[p.row][p.col];
    }

    private boolean positionIsExposed(Position p) {
        return p.row == nRows - 1
                || cards[p.row + 1][p.col] == null
                && cards[p.row + 1][p.col + 1] == null;
    }

    private Collection<Position> exposedPositions() {
        final ArrayList<Position> exposed = new ArrayList<>();
        for (Position p : allPositions()) {
            if (positionIsExposed(p) && cardAt(p) != null) {
                exposed.add(p);
            }
        }
        return exposed;
    }

    // Public non-mutating methods
    public Collection<Position> exposedKings() {
        final ArrayList<Position> kingPositions = new ArrayList<>(4);
        for (Position p : exposedPositions()) {
            if (cardAt(p).rank == 13) {
                kingPositions.add(p);
            }
        }
        return kingPositions;
    }

    public boolean isEmpty() {
        for (Position p : allPositions()) {
            if (cardAt(p) != null) {
                return false;
            }
        }
        return true;
    }

    public Iterable<Position> matchesFor(Card card) {
        final ArrayList<Position> matchPositions = new ArrayList<>();
        for (Position p : exposedPositions()) {
            if (cardAt(p).rank + card.rank == 13) {
                matchPositions.add(p);
            }
        }
        return matchPositions;
    }

    public Set<UnorderedPair<Position>> matchingExposedPairs() {
        final HashSet<UnorderedPair<Position>> pairs = new HashSet<>(12);  // max possible ~12 on std. board?
        for (Position p : exposedPositions()) {
            final Iterable<Position> matches = matchesFor(cardAt(p));
            for (Position q : matches) {
                final UnorderedPair<Position> pair = new UnorderedPair<>(p, q);
                pairs.add(pair);
            }
        }
        return pairs;
    }

    public int nCards() {
        return nCards;
    }

    public Board removeCardAt(Position p) {
        if (cardAt(p) == null) {
            throw new IllegalArgumentException(
                    String.format("position %s is empty", p)
            );
        }
        else if (!positionIsExposed(p)) {
            throw new IllegalArgumentException(String.format("Card at %s is not exposed", p));
        }
        final Board other = new Board(this);
        other.cards[p.row][p.col] = null;
        return other;
    }

    public Board removeCardsAt(Position p, Position q) {
        if (p.equals(q)) {
            throw new IllegalArgumentException(String.format("positions should not be equal (%s)", p));
        }
        else if (cardAt(p) == null) {
            throw new IllegalArgumentException(
                    String.format("position row %s is empty", p)
            );
        }
        else if (cardAt(q) == null) {
            throw new IllegalArgumentException(
                    String.format("position row %s is empty", q)
            );
        }
        else if (!positionIsExposed(p)) {
            throw new IllegalArgumentException(String.format("Card at %s is not exposed", p));
        }
        else if (!positionIsExposed(q)) {
            throw new IllegalArgumentException(String.format("Card at %s is not exposed", q));
        }
        final Board other = new Board(this);
        other.cards[p.row][p.col] = null;
        other.cards[q.row][q.col] = null;
        return other;
    }

    // Boilerplate
    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || getClass() != other.getClass()) {
            return false;
        }
        Board that = (Board) other;
        return Arrays.deepEquals(cards, that.cards);
    }

    @Override
    public int hashCode() {
        if (this.cachedHash == 0) {
            this.cachedHash = Arrays.deepHashCode(cards);
        }
        return this.cachedHash;
    }

    public String toString() {
        final String[] rowStrings = new String[nRows];
        final int rowStringLength = 4 * nRows - 2;
        for (int row = 0; row < nRows; ++row) {
            final StringJoiner joiner = new StringJoiner("  ");
            for (Card c : cards[row]) {
                if (c != null) {
                    joiner.add(c.toString());
                }
                else {
                    joiner.add("  ");
                }
            }
            final String unpadded = joiner.toString();
            final int padSize = (rowStringLength - unpadded.length()) / 2;
            final String pad = new String(new char[padSize]).replace("\0", " ");  // man I hate Java
            rowStrings[row] = pad + unpadded + pad;
        }
        return String.join("\n", rowStrings);
    }
}