package solitareGames;

/**
 * The interface to be implemented by all classes representing the state of a game.
 * <p>
 * Implementing classes should also override {@code equals} and {@code hashCode} in a manner that two
 * identical game states will be recognized as such even if they were arrived at via different permutations of the same
 * moves. This is very important to enable a pruning optimization employed in the solver.
 * <p>
 * @param <T>  the implementing class itself, i.e. {@code Foo implements GameState<Foo>}
 */
public interface GameState<T extends GameState> {

    /** Returns an estimate of the number of moves necessary to reach the solution state. */
    int estimatedCostToGoal();

    /**
     * Checks if this game state is the solved state (e.g., in Pyramid, if all cards have been removed).
     * <p>
     * @return  true if solved
     */
    boolean isGoal();

    /**
     * Obtains all game states which can be reached from this game state in a single legal move.
     * <p>
     * @return  Iterable containing all neighboring states; empty if no legal moves exist
     */
    Iterable<T> neighbors();
}
