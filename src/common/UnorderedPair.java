package common;

/**
 * A pair of items of the same type with no semantic order.
 * <p>
 * Equality and hashing are implemented such that {@code UnorderedPair<T>(foo, bar)}
 * will be considered identical to {@code UnorderedPair<T>(bar, foo)}.
 * <p>
 * Intended for use in containers like {@code HashSet} when only one of {A, B} or {B, A} should appear.
 * <p>
 * @param <T>  the type of items to be contained
 */
public class UnorderedPair<T> {

    public final T foo, bar;
    private int cachedHash;

    /**
     * Constructs an UnorderedPair containing the specified items.
     * <p>
     * @param foo  an item to be contained, not null
     * @param bar  an item to be contained, not null
     */
    public UnorderedPair(T foo, T bar) {
        if (foo == null || bar == null) {
            throw new NullPointerException("does not accept null");
        }
        this.foo = foo;
        this.bar = bar;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (other == null || other.getClass() != this.getClass()) {
            return false;
        }
        final UnorderedPair<?> that = (UnorderedPair<?>) other;  // avoids unchecked cast; semantically OK to me
        return (this.foo.equals(that.foo) && this.bar.equals(that.bar)  // equality without regard to order
                || this.foo.equals(that.bar) && this.bar.equals(that.foo));
    }

    @Override
    public int hashCode() {
        if (cachedHash == 0) {
            cachedHash = foo.hashCode() + bar.hashCode();  // hash without regard to order
        }
        return cachedHash;
    }
}
