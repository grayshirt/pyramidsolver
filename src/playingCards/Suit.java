package playingCards;

/**
 * Represents the four suits found in a standard deck of cards.
 */
public enum Suit {

    CLUBS ("clubs", Color.BLACK),
    DIAMONDS ("diamonds", Color.RED),
    HEARTS ("hearts", Color.RED),
    SPADES ("spades", Color.BLACK);

    /**
     * Represents the two colors a suit of cards may have.
     */
    public enum Color {
        BLACK, RED
    }

    public final String name;
    public final char initial;
    public final Color color;

    Suit(String name, Color color) {
        this.name = name;
        initial = name.charAt(0);
        this.color = color;
    }

    /**
     * Obtain the Suit value corresponding to the given initial character.
     * <p>
     * @param c  the initial character specifying the suit, 'c', 'd', 'h' or 's'
     * @return Suit value
     * @throws IllegalArgumentException if c invalid
     */
    public static Suit getByInitial(char c) {
        Suit suit;
        char i = Character.toLowerCase(c);
        if (i == 'c') {
            suit = CLUBS;
        }
        else if (i == 'd') {
            suit = DIAMONDS;
        }
        else if (i == 'h') {
            suit = HEARTS;
        }
        else if (i == 's') {
            suit = SPADES;
        }
        else {
            throw new IllegalArgumentException(String.format("'%s' invalid Suit initial (c, d, s, h valid)", c));
        }
        return suit;
    }

    public static final int N_SUITS = Suit.values().length;  // Initialize at end, after Suit values constructed
}