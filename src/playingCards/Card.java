package playingCards;

import java.util.ArrayList;
import java.util.Collection;

/**
 * A playing card from a standard 52-card deck.
 */
public class Card {

    public static final int MIN_RANK = 1, MAX_RANK = 13;
    public static final int N_UNIQUE_CARDS = ((MAX_RANK - MIN_RANK) + 1) * Suit.N_SUITS;

    public final int rank;
    public final Suit suit;

    private int cachedHash;

    // Constructors and factories

    /**
     * Constructs a Card of the given rank and suit.
     * <p>
     * @param rank  the rank of this card represented as number 1 to 13; 1=ace, 11=jack, 12=queen, 13=king
     * @param suit  the suit of this card, not null
     * @throws IllegalArgumentException if rank out-of-range
     */
    public Card(int rank, Suit suit) {
        if (suit == null) {
            throw new NullPointerException("suit should not be null");
        }
        if (MIN_RANK > rank || rank > MAX_RANK) {
            final String msg = String.format(
                    "Given rank %d is invalid; must be %d to %d, inclusive", rank, MIN_RANK, MAX_RANK
            );
            throw new IllegalArgumentException(msg);
        }
        this.rank = rank;
        this.suit = suit;
    }

    /**
     * Obtains the Card corresponding to the given two-character value.
     * <p>
     * Input is expected in the same format as the output of {@code Card.toString}. Use 'T' to represent a rank of 10.
     * <p>
     * @param chars  a sequence of exactly two characters, the first rank and the second suit; case-insensitive
     * @return the Card corresponding to the given string
     * @throws IllegalArgumentException if chars does not identify a valid Card
     */
    public static Card fromChars(CharSequence chars) {
        if (chars == null) {
            throw new NullPointerException();
        }
        else if (chars.length() != 2) {
            throw new IllegalArgumentException(String.format("'%s' invalid initializer for Card", chars));
        }
        char rankChar = chars.charAt(0), suitChar = chars.charAt(1);
        final int rank = getRank(rankChar);
        final Suit suit = Suit.getByInitial(suitChar);
        return new Card(rank, suit);
    }

    /**
     * Obtains all 52 cards found in a standard deck.
     * <p>
     * @return {@code Collection<Card>}
     */
    public static Collection<Card> allUniqueCards() {
        final ArrayList<Card> cards = new ArrayList<>(N_UNIQUE_CARDS);
        for (Suit suit : Suit.values()) {
            for (int rank = Card.MIN_RANK; rank <= Card.MAX_RANK; ++rank) {
                cards.add(new Card(rank, suit));
            }
        }
        return cards;
    }

    // Internals
    private static int getRank(char rankChar) {
        final int rank, MIN_RANK_DIGIT = 2, MAX_RANK_DIGIT = 9;
        if (Character.isDigit(rankChar)) {
            rank = Character.getNumericValue(rankChar);
            if (rank < MIN_RANK_DIGIT || rank > MAX_RANK_DIGIT) {
                throw new IllegalArgumentException(String.format("%s invalid rank for Card", rankChar));
            }
        }
        else {
            char i = Character.toUpperCase(rankChar);
            if (i == 'A') {
                rank = 1;
            }
            else if (i == 'K') {
                rank = 13;
            }
            else if (i == 'Q') {
                rank = 12;
            }
            else if (i == 'J') {
                rank = 11;
            }
            else if (i == 'T') {
                rank = 10;
            }
            else {
                throw new IllegalArgumentException(String.format("%s invalid rank for Card", rankChar));
            }
        }
        return rank;
    }

    private String rankToString() {
        final String c;
        if (rank == 13) {
            c = "K";
        }
        else if (rank == 12) {
            c = "Q";
        }
        else if (rank == 11) {
            c = "J";
        }
        else if (rank == 10) {
            c = "T";
        }
        else if (rank == 1) {
            c = "A";
        }
        else {
            c = Integer.toString(rank);
        }
        return c;
    }

    // Boilerplate
    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || getClass() != other.getClass()) {
            return false;
        }
        Card that = (Card) other;
        return rank == that.rank && suit == that.suit;
    }

    @Override
    public int hashCode() {
        if (cachedHash == 0) {
            int result = rank;
            result = 31 * result + suit.hashCode();
            cachedHash = result;
        }
        return cachedHash;
    }

    public String toString() {
        return String.format("%s%s", rankToString(), suit.initial);
   }
}