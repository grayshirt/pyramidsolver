package playingCards;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.NoSuchElementException;

/**
 * A deck of {@link Card}s.
 */
public class Deck {

    private int cachedHash;
    private final LinkedList<Card> cards;  // add/removeFirst for top of deck, add/removeLast for bottom
    private static final String stringDelimiter = ",";

    // Constructors and static factories

    /**
     * Constructs an empty Deck.
     */
    public Deck() {
        cards = new LinkedList<>();
    }

    /**
     * Constructs a Deck containing the given Cards.
     * <p>
     * @param initialCards  the collection containing the Cards this Deck will contain
     */
    public Deck(Collection<Card> initialCards) {
        this();
        cards.addAll(initialCards);
    }

    /**
     * Constructs a copy of another Deck.
     * <p>
     * @param other  the Deck to be copied
     */
    public Deck(Deck other) {
        this(other.cards);
    }

    /**
     * Obtains a Deck containing the Cards specified by the given card.
     * <p>
     * Input is expected in the same format that {@code Deck.toString()} outputs; e,g. "As, 2d, 3h, Tc"
     * @param chars  a comma-delimited string of two-character sequences specifying cards, whitespace ignored
     * @return Deck
     * @throws IllegalArgumentException if any element does not identify a valid card
     */
    public static Deck fromCharSequence(CharSequence chars) {
        if (chars == null) {
            throw new NullPointerException();
        }
        String deckString = chars.toString();
        String[] cardStrings = deckString.split(stringDelimiter);
        ArrayList<Card> initialCards = new ArrayList<>();
        for (String cardString : cardStrings) {
            initialCards.add(Card.fromChars(cardString.trim()));
        }
        return new Deck(initialCards);
    }

    /**
     * Obtain a shuffled, standard 52-card deck.
     * <p>
     * @return Deck
     */
    public static Deck getShuffledDeck() {
        final ArrayList<Card> cards = new ArrayList<>(Card.allUniqueCards());
        Collections.shuffle(cards);
        return new Deck(cards);
    }

    /**
     * Obtain an unshuffled standard 52-card deck.
     * <p>
     * @return Deck
     */
    public static Deck getUnshuffledDeck() {
        return new Deck(Card.allUniqueCards());
    }

    // Boilerplate
    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || getClass() != other.getClass()) {
            return false;
        }
        Deck that = (Deck) other;
        return cards.equals(that.cards);
    }

    @Override
    public int hashCode() {
        if (cachedHash == 0) {
            cachedHash = cards.hashCode();
        }
        return cachedHash;
    }

    public String toString() {
        final ArrayList<String> cardStrings = new ArrayList<>(size());
        for (Card c : cards) {
            cardStrings.add(c.toString());
        }
        return String.join(", ", cardStrings);
    }

    // Public non-mutating methods

    /**
     * Returns true if this Deck contains no cards.
     * <p>
     * @return boolean
     */
    public boolean isEmpty() {
        return cards.isEmpty();
    }

    /**
     * Returns the bottom card of this Deck, or null if empty.
     * <p>
     * @return Card
     */
    public Card peekBottom() {
        return cards.peekLast();
    }

    /**
     * Returns the top card of this Deck, or null if empty.
     * <p>
     * @return Card
     */
    public Card peekTop() {
        return cards.peekFirst();
    }

    /**
     * Returns the number of Cards in this Deck.
     * <p>
     * @return int
     */
    public int size() {
        return cards.size();
    }

    // Public mutating methods

    /**
     * Adds the given Card to the bottom of this Deck.
     * <p>
     * @param card  the Card to add
     */
    public void addBottom(Card card) {
        cards.addLast(card);
    }

    /**
     * Adds the given Card to the top of this Deck.
     * <p>
     * @param card  the Card to add
     */
    public void addTop(Card card) {
        cards.addFirst(card);
    }

    /**
     * Removes and returns the bottom Card of this Deck.
     * <p>
     * @return  Card
     * @throws NoSuchElementException if Deck is empty
     */
    public Card dealBottom() {
        return cards.removeLast();
    }

    /**
     * Removes and returns the top Card of this Deck.
     * <p>
     * @return  Card
     * @throws NoSuchElementException if Deck is empty
     */
    public Card dealTop() {
        return cards.removeFirst();
    }

    /**
     * Removes and returns the given number of Cards from the top of this Deck.
     * @param nCards  the number of Cards to be removed
     * @return {@code Collection<Card>}
     * @throws NoSuchElementException if nCards is greater than the size of this Deck
     */
    public Collection<Card> dealTop(int nCards) {
        if (nCards > size()) {
            throw new NoSuchElementException(String.format("%d cards requested when deck contains %d", nCards, size()));
        }
        final ArrayList<Card> dealt = new ArrayList<>(nCards);
        for (int i = 0; i < nCards; ++i) {
            dealt.add(dealTop());
        }
        return dealt;
    }
}
