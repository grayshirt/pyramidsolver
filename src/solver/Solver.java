package solver;

import solitareGames.GameState;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.PriorityQueue;

/**
 * Finds solutions to simple games.
 * <p>
 * @param <T>  the game state type which defines the game to be solved
 */
class Solver<T extends GameState<T>> {

    private class Node implements Comparable<Node> {

        private final T state;
        private final Node previousNode;
        private final int moves;

        Node(T state, Node previousNode, int moves) {
            this.state = state;
            this.previousNode = previousNode;
            this.moves = moves;
        }

        public int compareTo(Node that) {
            return Integer.compare(this.totalCostEstimate(), that.totalCostEstimate());
        }

        private int totalCostEstimate() {
            return state.estimatedCostToGoal() + moves;
        }
    }

    private final LinkedList<T> solutionStates;
    public final double timeInSeconds;

    /**
     * Initiates the search for a path to a solution from the given initial game state.
     * <p>
     * @param initialState  the game state from which to begin the search
     */
    public Solver(T initialState) {
        // Initialize
        final PriorityQueue<Node> pq = new PriorityQueue<>();
        final Node initialNode = new Node(initialState, null, 0);
        final long startTime = System.currentTimeMillis();
        pq.add(initialNode);
        // Solve
        Node currentNode = pq.poll();
        HashSet<Integer> visitedStateHashes = new HashSet<>();
        while (!currentNode.state.isGoal()) {
            if (!visitedStateHashes.contains(currentNode.state.hashCode())) {
                visitedStateHashes.add(currentNode.state.hashCode());
                for (T neighbor : currentNode.state.neighbors()) {
                    Node neighborNode = new Node(neighbor, currentNode, currentNode.moves + 1);
                    pq.add(neighborNode);
                }
            }
            if (!pq.isEmpty()) {
                currentNode = pq.poll();
            }
            else {
                break;
            }
        }
        // Store solution, if found
        if (currentNode.state.isGoal()) {
            solutionStates = new LinkedList<>();
            do {
                solutionStates.addFirst(currentNode.state);
                currentNode = currentNode.previousNode;
            } while (currentNode != null);
        }
        else {
            solutionStates = null;
        }
        final long endTime = System.currentTimeMillis();
        timeInSeconds = (endTime - startTime) * 1e-3;
    }

    /**
     * Checks if a solution was found.
     * <p>
     * @return  true if game was solved
     */
    public boolean isSolvable() {
        return (solutionStates != null);
    }

    /**
     * Gets the number of steps taken in the generated solution, if any.
     * <p>
     * @return non-negative integer if solution found, -1 if not
     */
    public int moves() {
        return (solutionStates != null) ? solutionStates.size() - 1 : -1;
    }

    /**
     * Gets the series of game states representing the path from the given state to a solution.
     * <p>
     * @return Iterable of game states if solution found; null if not
     */
    public Iterable<T> solution() {
        return solutionStates;
    }
}