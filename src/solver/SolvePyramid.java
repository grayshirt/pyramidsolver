package solver;

import playingCards.Deck;
import solitareGames.pyramidSolitare.PyramidConsts;
import solitareGames.pyramidSolitare.PyramidGameState;

class SolvePyramid {
    public static void main(String[] args) {
        Deck deck = null;
        if (args.length == 0) {
            deck = Deck.getShuffledDeck();
        }
        else if (args.length == 1) {
            deck = Deck.fromCharSequence(args[0]);
        }
        else {
            System.out.println("Deck argument, if given, is a single string (enclosed in quotes or without whitespace)");
            System.exit(-1);
        }
        System.out.println("Deck:\n" + deck);
        PyramidGameState initialState = new PyramidGameState(
            deck, PyramidConsts.N_ROWS_STANDARD, PyramidConsts.STOCK_CYCLES_RELAXED
        );
        Solver<PyramidGameState> solver = new Solver<>(initialState);
        if (solver.isSolvable()) {
            System.out.println("Won!");
            int i = 0;
            for (PyramidGameState state : solver.solution()) {
                System.out.println(i++);
                System.out.println(state);
            }
        }
        else {
            System.out.println("Unsolved!");
        }
        System.out.println(String.format("Completed in %.2f seconds", solver.timeInSeconds));
    }
}