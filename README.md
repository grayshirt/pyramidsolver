# Pyramid Solver

## What is it?
A solver for the [Pyramid](https://en.wikipedia.org/wiki/Pyramid_%28solitaire%29) and 
[Tripeaks](https://en.wikipedia.org/wiki/Tri_Peaks) types of Solitare card games.

## What are the rules of the game implemented in this solver?
* __Pyramid__: emptying only the Pyramid, not necessarily the Stock, is considered sufficient for a win; option to
allow stock recycles
* __Tripeaks__: no scoring

## What's the algorithm?
A simple implementation of [A*](https://en.wikipedia.org/wiki/A*_search_algorithm) employing a priority queue, with 
game states ranked by a few crude measures of their distance from and potential for a solution. An easy optimization 
avoiding repeatedly searching the same game state (which can be arrived at via different permutations of the same moves)
greatly improves performance.

## Does it work?
It solves many games of standard size in a few seconds; for others, it runs for minutes without terminating. No claim 
is made as to whether it solves every solvable game or whether generated solutions are, in fact, the fastest possible.
Such a claim would be possible if one could state that the heuristic used to prioritize game state nodes in the search 
for a solution was "[admissible](https://en.wikipedia.org/wiki/Admissible_heuristic)". I do not know if the one used 
here is.

Solving Tripeaks games will occasionally result in an out-of-memory error; this can be avoided by launching the JVM 
with an option like "-Xmx1200m".

## Potential improvements
* Improve general code style, employing more idiomatic Java and better practices including documentation, interfaces, 
packaging and testing
* Consider early dead-end detection (e.g., in Pyramid, a complete stock cycle during which no moves were made)

## License
This code is offered under the MIT License.